module.exports = {
    publicPath: '',
    pages: {
        index: {
            entry: 'src/main.js',
            template: "public/index.html",
            title: "YM-Downloader",
        },

        signin: {
            entry: "src/signin.js",
            template: "public/signin.html",
            title: "Sign in | YM-Downloader",
        },

        signup: {
            entry: "src/signup.js",
            template: "public/signup.html",
            title: "Sign up | YM-Downloader",
        },
        app: {
            entry: "src/app.js",
            template: "public/app.html",
            title: "YM-Downloader",
        }
    },


    runtimeCompiler: true,

    chainWebpack: (config) => {
        config.resolve.symlinks(false)
    }
};