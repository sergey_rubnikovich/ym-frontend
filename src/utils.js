export function getDataPromise(ajaxFunc, ...args) {
    return new Promise((resolve) => {
        ajaxFunc(resolve, ...args);
    });
}
